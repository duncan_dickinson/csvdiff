package name.dickinson.duncan.csv

import java.sql.ResultSet;

import au.com.bytecode.opencsv.CSVWriter

class Writer extends CSVWriter {
	static void writeAll(
			ResultSet resultSet,
			String file,
			boolean includeColumnNames = true,
			CSVProperties properties = new CSVProperties()){
		FileWriter f = new FileWriter(file)
		CSVWriter writer = new CSVWriter(f, 
			properties.separator, 
			properties.quotechar,
			properties.escapechar,
			properties.lineEnd);
		writer.writeAll(resultSet, includeColumnNames)
		writer.close()
	}
}
