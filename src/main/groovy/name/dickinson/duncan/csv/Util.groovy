package name.dickinson.duncan.csv;

public class Util {

	static boolean compareList(List list1, List list2) {
		if (list1.size() != list2.size())
			return false

		def l1 = new ArrayList(list1)
		def l2 = new ArrayList(list2)
		Collections.sort(l1)
		Collections.sort(l2)
		if (l1.equals(l2))
			return true
		return false
	}

}
