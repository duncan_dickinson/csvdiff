package name.dickinson.duncan.csv

import au.com.bytecode.opencsv.CSVReader

@groovy.transform.Immutable(knownImmutableClasses = [CSVProperties])
@groovy.util.logging.Log
public class CSV {

	String fileName
	CSVProperties properties

	synchronized String[] getColumns(){
		FileReader f = new FileReader(this.fileName)
		CSVReader reader = new CSVReader(f,
				this.properties.separator,
				this.properties.quotechar,
				this.properties.escapechar,
				0,
				this.properties.strictQuotes,
				this.properties.ignoreLeadingWhiteSpace)
		String[] columns = reader.readNext()
		reader.close()
		return columns
	}

	synchronized String[] getEntries(){
		CSVReader reader = new CSVReader(f,
				this.properties.separator,
				this.properties.quotechar,
				this.properties.escapechar,
				this.properties.ignoreLines,
				this.properties.strictQuotes,
				this.properties.ignoreLeadingWhiteSpace)
		String[] entries = reader.readAll()
		reader.close()
		return entries
	}

}
