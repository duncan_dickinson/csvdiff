package name.dickinson.duncan.csv

import au.com.bytecode.opencsv.CSVParser
import au.com.bytecode.opencsv.CSVWriter

@groovy.transform.Immutable
class CSVProperties {
	char separator = CSVParser.DEFAULT_SEPARATOR
	char quotechar = CSVParser.DEFAULT_QUOTE_CHARACTER
	char escapechar = CSVParser.DEFAULT_ESCAPE_CHARACTER
	String lineEnd = CSVWriter.DEFAULT_LINE_END
	
	//Assumes that the first line is a header row
	int ignoreLines = 1
	
	boolean strictQuotes = CSVParser.DEFAULT_STRICT_QUOTES
	boolean ignoreLeadingWhiteSpace = CSVParser.DEFAULT_IGNORE_LEADING_WHITESPACE
	
	//Just a common name for the ID column :)
	String idColumn = 'ID'
}
